from unittest import TestCase

import pandas as pd

from DataUtility import (
    get_borough_complaints,
    get_complaint_index,
    get_complaints_online,
    get_most_populous_zip_codes,
    get_top_complaints,
    get_zip_code_complaints
)


class DataFetcherTest(TestCase):

    def test_get_complaints_online(self):
        df = get_complaints_online()
        self.assertIsInstance(df, pd.DataFrame)
        if not df.empty:
            self.assertGreater(len(df), 0)

    def test_get_top_complaints(self):
        data = [['A', 1], ['A', 2], ['B', 3], ['C', 4], ['A', 5], ['B', 6]]
        df = pd.DataFrame(data=data, columns=['complaint_type', 'unique_key'])
        self.assertDictEqual({'B': 2, 'A': 3}, get_top_complaints(df, 2))

    def test_get_most_populous_zip_codes(self):
        data = [[10000, 1], [10002, 4], [10005, 3], [10006, 2]]
        df = pd.DataFrame(data=data, columns=['Zip Code ZCTA', '2010 Census Population'])
        self.assertDictEqual({10002: 4, 10005: 3}, get_most_populous_zip_codes(df, 2))

    def test_get_borough_complaints(self):
        data = [['A', 1], ['A', 2], ['C', 3], ['A', 1]]
        df = pd.DataFrame(data=data, columns=['borough', 'complaint_type'])
        results = get_borough_complaints(df, [1, 2])
        self.assertNotIn('C', results['borough'].unique())
        self.assertIn('A', results['borough'].unique())
        self.assertEqual(2, results[(results['borough'] == 'A') & (results['complaint_type'] == 1)]['count'].sum())

    def test_get_zip_code_complaints(self):
        data = [['A', 1], ['A', 2], ['C', 3], ['A', 1]]
        df = pd.DataFrame(data=data, columns=['complaint_type', 'incident_zip'])
        results = get_zip_code_complaints(df, ['A'], [1, 2])
        self.assertNotIn('C', results['complaint_type'].unique())
        self.assertIn('A', results['complaint_type'].unique())
        self.assertEqual(2, results[(results['complaint_type'] == 'A') & (results['incident_zip'] == 1)]['count'].sum())

    def test_get_complaint_index(self):
        data = [['A', 10000, ''], ['A', 10002, ''], ['C', 10000, ''], ['A', 10000, '']]
        complaints_df = pd.DataFrame(data=data, columns=['borough', 'incident_zip', 'filler'])

        data = [[10000, 1], [10002, 4], [10005, 3], [10006, 2]]
        population_df = pd.DataFrame(data=data, columns=['Zip Code ZCTA', '2010 Census Population'])
        results = get_complaint_index(complaints_df, population_df)
        self.assertAlmostEqual(.6, results[(results['borough'] == 'A')]['complaint_index'].sum(), 1)
        self.assertAlmostEqual(1.0, results[(results['borough'] == 'C')]['complaint_index'].sum(), 1)
