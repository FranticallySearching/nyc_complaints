import pandas as pd
from sodapy import Socrata


def get_complaints_online()->pd.DataFrame:
    """ Return complaint data from online source using an unauthenticated client """
    client = Socrata("data.cityofnewyork.us", None)
    return pd.DataFrame.from_records(client.get("fhrw-4uyv"))


def get_top_complaints(df: pd.DataFrame, n: int)->dict:
    """ Return dict of the top n most common complain_types (k) to their counts (v) """
    count_df = df.groupby(['complaint_type'])['unique_key'].count().reset_index(name='count')
    count_df = count_df.nlargest(n, 'count')[['complaint_type', 'count']]
    return count_df.set_index('complaint_type').to_dict()['count']


def get_most_populous_zip_codes(df: pd.DataFrame, n: int)->dict:
    """ Return dict of the top n most populous zip codes (k) to their population size (v) """
    count_df = df.nlargest(n, '2010 Census Population')
    return count_df.set_index('Zip Code ZCTA').to_dict()['2010 Census Population']


def get_borough_complaints(df: pd.DataFrame, complaints: list)->pd.DataFrame:
    """ Return a DataFrame with a count of each boroughs complaints given a list of complaints """
    complaints_df = df[df['complaint_type'].isin(complaints)]
    return complaints_df.groupby(['borough', 'complaint_type'])['complaint_type'].count().reset_index(name='count')


def get_zip_code_complaints(df: pd.DataFrame, complaints: list, zip_codes: list)->pd.DataFrame:
    """ Return a DataFrame with a count of each complaint given for each zip code given """
    complaints_df = df[df['complaint_type'].isin(complaints) & df['incident_zip'].isin(zip_codes)]
    count_df = complaints_df.groupby(['incident_zip', 'complaint_type'])['complaint_type'].count().reset_index(name='count')
    return count_df


def get_complaint_index(complaints_df: pd.DataFrame, census_df: pd.DataFrame)->pd.DataFrame:
    """ Return a DataFrame with a complaint index sorted by most complaints proportional to borough population """
    drop_cols = set(complaints_df.columns).difference({'borough', 'incident_zip'})
    boroughs = complaints_df.drop(labels=drop_cols, axis=1).drop_duplicates()
    zip_boroughs = census_df.merge(boroughs, how='inner', left_on='Zip Code ZCTA', right_on='incident_zip')
    borough_pop = zip_boroughs.groupby(['borough'])['2010 Census Population'].sum().reset_index(name='pop_count')

    complaints = complaints_df.groupby(['borough'])['borough'].count().reset_index(name='complaint_count')
    ratio = complaints.merge(borough_pop, how='inner', on='borough')
    ratio['complaint_index'] = ratio['complaint_count'] / ratio['pop_count']
    return ratio.sort_values(by='complaint_index', ascending=False)
